set schema 'trainSNCF';

CREATE TABLE TRAIN
(
	numtrain INTEGER NOT NULL PRIMARY KEY,
	type VARCHAR(12)
);

CREATE TABLE VOITURE
( 
	numvoiture INTEGER NOT NULL,
	numtrain INTEGER NOT NULL,
	etage BOOL,
	PRIMARY KEY(numvoiture,numtrain),
	FOREIGN KEY (numtrain) REFERENCES TRAIN
);

CREATE TABLE PLACE
(
	numplace DECIMAL NOT NULL,
	numvoiture INTEGER NOT NULL,
	numtrain INTEGER NOT NULL,
	categorie VARCHAR(50),
	PRIMARY KEY (numplace,numvoiture,numtrain),
	FOREIGN KEY (numvoiture,numtrain) REFERENCES VOITURE
);



CREATE TABLE GARE
(
	numgare SERIAL NOT NULL PRIMARY KEY,
	ville VARCHAR(50)
);

CREATE TABLE TRAJET
(
 numtrajet VARCHAR(50),
 date_depart DATE,
 heure_depart TIME,
 date_arrive DATE,
 heure_arrive TIME,
 gare_arrive INTEGER NOT NULL,
 gare_depart INTEGER NOT NULL,
 numtrain INTEGER NOT NULL,
 PRIMARY KEY (numtrajet),
 FOREIGN KEY (numtrain) REFERENCES TRAIN,
 FOREIGN KEY (gare_arrive) REFERENCES GARE,
 FOREIGN KEY (gare_depart) REFERENCES GARE
);


CREATE TABLE ETAPE
(
 numtrajet VARCHAR(50),
 numgare INTEGER NOT NULL,
 horaire_etape TIME,
 PRIMARY KEY (numtrajet, numgare),
 FOREIGN KEY (numtrajet) REFERENCES TRAJET, 
 FOREIGN KEY (numgare) REFERENCES GARE
);


CREATE TABLE RESERVATION
(
 numtrajet VARCHAR(50),
 numplace DECIMAL NOT NULL,
 numvoiture INTEGER NOT NULL,
 numtrain INTEGER NOT NULL,
 nomclient VARCHAR(50),
 date_reservation DATE,
 PRIMARY KEY (numtrajet,numplace,numvoiture,numtrain),
 FOREIGN KEY (numtrajet) REFERENCES TRAJET,
 FOREIGN KEY (numplace,numvoiture,numtrain) REFERENCES PLACE
);

insert into TRAIN values (1234,'TER');
insert into TRAIN values (2345,'TGV');
insert into TRAIN values (3456,'OUI');
insert into TRAIN values (4567,'Thello');
insert into TRAIN values (5678,'Eurostar');
insert into TRAIN values (6789,'AVE Elipsos');
insert into TRAIN values (9876,'TER');
insert into TRAIN values (8765,'FlixBus');
insert into TRAIN values (7654,'Hello');
insert into TRAIN values (6543,'ICBus');
insert into TRAIN values (5432,'ICN');
insert into TRAIN values (4321,'IC');
insert into TRAIN values (3210,'Thello');
insert into TRAIN values (5532,'TGV');

insert into VOITURE values (1,1234,true);
insert into VOITURE values (2,1234,true);
insert into VOITURE values (3,1234,true);
insert into VOITURE values (4,1234,true);
insert into VOITURE values (5,1234,true);
insert into VOITURE values (1,2345,true);
insert into VOITURE values (2,2345,true);
insert into VOITURE values (3,2345,true);
insert into VOITURE values (4,2345,true);
insert into VOITURE values (5,2345,true);
insert into VOITURE values (6,2345,true);
insert into VOITURE values (7,2345,true);
insert into VOITURE values (8,2345,true);
insert into VOITURE values (9,2345,true);
insert into VOITURE values (10,2345,true);
insert into VOITURE values (11,2345,true);
insert into VOITURE values (1,3456,false);
insert into VOITURE values (2,3456,false);
insert into VOITURE values (3,3456,false);
insert into VOITURE values (4,3456,false);
insert into VOITURE values (5,3456,false);
insert into VOITURE values (6,3456,false);
insert into VOITURE values (7,3456,false);
insert into VOITURE values (1,4567,false);
insert into VOITURE values (2,4567,false);
insert into VOITURE values (3,4567,false);
insert into VOITURE values (1,5678,true);
insert into VOITURE values (2,5678,true);
insert into VOITURE values (3,5678,true);
insert into VOITURE values (4,5678,true);
insert into VOITURE values (5,5678,true);
insert into VOITURE values (1,6789,true);
insert into VOITURE values (2,6789,true);
insert into VOITURE values (3,6789,true);
insert into VOITURE values (4,6789,true);
insert into VOITURE values (5,6789,true);
insert into VOITURE values (6,6789,true);
insert into VOITURE values (7,6789,true);
insert into VOITURE values (8,6789,true);
insert into VOITURE values (1,9876,false);
insert into VOITURE values (2,9876,false);
insert into VOITURE values (3,9876,false);
insert into VOITURE values (4,9876,false);
insert into VOITURE values (5,9876,false);
insert into VOITURE values (1,8765,false);
insert into VOITURE values (1,7654,false);
insert into VOITURE values (1,6543,false);
insert into VOITURE values (1,5432,false);
insert into VOITURE values (2,5432,false);
insert into VOITURE values (3,5432,false);
insert into VOITURE values (4,5432,false);
insert into VOITURE values (5,5432,false);
insert into VOITURE values (6,5432,false);
insert into VOITURE values (7,5432,false);
insert into VOITURE values (8,5432,false);
insert into VOITURE values (9,5432,false);
insert into VOITURE values (1,4321,false);
insert into VOITURE values (2,4321,false);
insert into VOITURE values (3,4321,false);
insert into VOITURE values (4,4321,false);
insert into VOITURE values (5,4321,false);
insert into VOITURE values (6,4321,false);
insert into VOITURE values (7,4321,false);
insert into VOITURE values (8,4321,false);
insert into VOITURE values (9,4321,false);
insert into VOITURE values (1,5532,false);
insert into VOITURE values (2,5532,false);
insert into VOITURE values (3,5532,false);
insert into VOITURE values (4,5532,false);
insert into VOITURE values (5,5532,false);
insert into VOITURE values (6,5532,false);
insert into VOITURE values (7,5532,false);
insert into VOITURE values (8,5532,false);
insert into VOITURE values (9,5532,false);
insert into VOITURE values (10,5532,true);
insert into VOITURE values (11,5532,true);
insert into VOITURE values (12,5532,false);
insert into VOITURE values (14,5532,false);

insert into PLACE values (5,1,2345,'eco'),(6,1,2345,'eco'),(2,1,2345,'confort'),(3,2,2345,'confort'),(4,2,2345,'business'),(5,2,2345,'normal'),  (6,2,2345,'eco'),(7,2,2345,'eco'),(6,3,2345,'eco'),(1,3,2345,'plein tarif'),(1,4,2345,'confort'),(1,5,2345,'eco');
insert into PLACE values (1,1,3456,'eco'),(2,1,3456,'confort'),(3,1,3456,'eco'),(4,1,3456,'confort'),(5,1,3456,'confort'),(6,1,3456,'business'),(7,1,3456,'normal');
insert into PLACE values (1,1,4567,'eco'),(1,2,4567,'eco'),(1,3,4567,'eco'); 
insert into PLACE values (1,1,9876,'eco'),(2,1,9876,'eco'),(3,1,9876,'confort'),(6,1,9876,'confort'),(4,1,9876,'eco'),(5,1,9876,'business'), (1,2,9876,'confort'),(2,2,9876,'eco'),(3,2,9876,'normal'),(4,2,9876,'business'),(5,2,9876,'eco'),(1,3,9876,'normal'),(2,3,9876,'plein tarif'),
(1,4,9876,'confort'),(2,4,9876,'eco'),(1,5,9876,'eco'),(2,5,9876,'eco'),(3,5,9876,'eco'),(4,5,9876,'confort'),(5,5,9876,'confort'),(6,5,9876,'normal'),(7,5,9876,'business'),(8,5,9876,'plein tarif'),(9,5,9876,'eco'),(10,5,9876,'eco');
insert into PLACE values (1,1,8765,'eco'),(2,1,8765,'confort'),(3,1,8765,'eco'),(4,1,8765,'business'),(5,1,8765,'plein tarif'),(6,1,8765,'eco'),(7,1,8765,'normal');
insert into PLACE values (1,1,7654,'eco'),(2,1,7654,'eco'),(3,1,7654,'confort'),(4,1,7654,'confort'),(5,1,7654,'business'),(6,1,7654,'business'),(7,1,7654,'normal'),(8,1,7654,'normal'),(9,1,7654,'plein tarif'),(10,1,7654,'plein tarif'),(11,1,7654,'demi tarif'),(12,1,7654,'demi tarif'),(14,1,7654,'eco');

insert into GARE (ville) values ('Le Mans');
insert into GARE (ville) values ('Paris Montparnasse');
insert into GARE (ville) values ('Paris Est');
insert into GARE (ville) values ('Paris Lyon');
insert into GARE (ville) values ('Paris Nord');
insert into GARE (ville) values ('Gap');
insert into GARE (ville) values ('Marseille');
insert into GARE (ville) values ('Dole');
insert into GARE (ville) values ('Dijon');
insert into GARE (ville) values ('Besançon');
insert into GARE (ville) values ('Nancy');
insert into GARE (ville) values ('Lyon');
insert into GARE (ville) values ('Grenoble');
insert into GARE (ville) values ('Nantes');
insert into GARE (ville) values ('Brest');
insert into GARE (ville) values ('Lille');
insert into GARE (ville) values ('Belfort');
insert into GARE (ville) values ('Strasbourg');
insert into GARE (ville) values ('Annecy');
insert into GARE (ville) values ('Annemasse');

insert into TRAJET values ('2017A344', '2017/01/01', '10:00', '2017/01/01','10:56',2,1,2345);
insert into TRAJET values ('2017A345', '2017/02/01', '09:00', '2017/02/01','11:11',7,6,3456);
insert into TRAJET values ('2017A346', '2017/03/01', '12:22', '2017/03/01','14:25',7,6,4567);
insert into TRAJET values ('2017A347', '2017/03/02', '13:50', '2017/03/02','15:30',7,6,9876);
insert into TRAJET values ('2017A348', '2017/03/03', '14:00', '2017/03/03','15:46',7,6,8765);
insert into TRAJET values ('2017A349', '2017/03/04', '11:30', '2017/03/04','12:56',7,6,7654);
insert into TRAJET values ('2017A343', '2017/04/05', '10:00', '2017/04/05','10:45',10,8,1234);


insert into ETAPE values ('2017A344', 12, '10:20');
insert into ETAPE values ('2017A344', 13, '10:33');
insert into ETAPE values ('2017A345', 14, '09:30');
insert into ETAPE values ('2017A345', 15, '09:55');
insert into ETAPE values ('2017A345', 16, '10:20');
insert into ETAPE values ('2017A345', 17, '10:50');
insert into ETAPE values ('2017A347', 13, '14:20');
insert into ETAPE values ('2017A347', 16, '14:50');
insert into ETAPE values ('2017A347', 17, '15:05');
insert into ETAPE values ('2017A347', 18, '15:17');
insert into ETAPE values ('2017A349', 13, '11:50');
insert into ETAPE values ('2017A349', 15, '12:09');
insert into ETAPE values ('2017A349', 17, '12:33');
insert into ETAPE values ('2017A349', 19, '10:20');

insert into RESERVATION values ('2017A344', 5,1,2345,'Yoda','2017/01/01'),('2017A344', 2,1,2345,'Yoda','2017/01/01'),('2017A344', 3,2,2345,'Luke Skywalker','2017/01/02'),('2017A344', 5,2,2345,'Leia Organa','2017/01/03'),('2017A344', 1,5,2345,'Dark Vador','2017/01/04');

